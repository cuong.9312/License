package com.fis.cdd.license;

import java.io.IOException;

public class GetMacAddress {
	public static void main(String[] args) throws IOException {
		System.out.println(getMac());
	}

	public static String getMac() throws java.io.IOException {
		Process p = Runtime.getRuntime().exec("getmac /fo csv /nh");
		java.io.BufferedReader in = new java.io.BufferedReader(new java.io.InputStreamReader(p.getInputStream()));
		String line;
		line = in.readLine();
		String[] result = line.split(",");
		return result[0].replace("\"", "").trim();
	}

}
