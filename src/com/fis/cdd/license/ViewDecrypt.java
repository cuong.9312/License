package com.fis.cdd.license;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.StringTokenizer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.UIManager;

public class ViewDecrypt {

	private JFrame frmDecrypt;
	private JLabel lblKeygen;
	private JTextField txtKeyGen;
	private JButton btnDecrypt;
	private JLabel lblMacAddress;
	private JTextField txtMac;
	private JLabel lblNewLabel;
	private JTextField txtExpiredate;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					ViewDecrypt window = new ViewDecrypt();
					window.frmDecrypt.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ViewDecrypt() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		this.frmDecrypt = new JFrame();
		this.frmDecrypt.setTitle("Decrypt");
		this.frmDecrypt.setBounds(100, 100, 450, 187);
		this.frmDecrypt.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frmDecrypt.getContentPane().setLayout(null);
		this.frmDecrypt.getContentPane().add(getLblKeygen());
		this.frmDecrypt.getContentPane().add(getTxtKeyGen());
		this.frmDecrypt.getContentPane().add(getBtnDecrypt());
		this.frmDecrypt.getContentPane().add(getLblMacAddress());
		this.frmDecrypt.getContentPane().add(getTxtMac());
		this.frmDecrypt.getContentPane().add(getLblNewLabel());
		this.frmDecrypt.getContentPane().add(getTxtExpiredate());
	}

	public JLabel getLblKeygen() {
		if (lblKeygen == null) {
			lblKeygen = new JLabel("KeyGen");
			lblKeygen.setBounds(10, 11, 46, 14);
		}
		return lblKeygen;
	}

	public JTextField getTxtKeyGen() {
		if (txtKeyGen == null) {
			txtKeyGen = new JTextField();
			txtKeyGen.setBounds(88, 8, 298, 20);
			txtKeyGen.setColumns(10);
		}
		return txtKeyGen;
	}

	public JButton getBtnDecrypt() {
		if (btnDecrypt == null) {
			btnDecrypt = new JButton("Decrypt");
			btnDecrypt.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String keyGen = txtKeyGen.getText();
					DesEncrypter encrypter = new DesEncrypter();
					String plainText = encrypter.decrypt(keyGen);
					StringTokenizer stk = new StringTokenizer(plainText, "_");
					String macAddress = stk.nextToken();
					String date = stk.nextToken() + "/" + (Integer.parseInt(stk.nextToken()) + 1) + "/" + stk.nextToken();
					txtMac.setText(macAddress);
					txtExpiredate.setText(date);
				}
			});
			btnDecrypt.setBounds(88, 39, 89, 23);
		}
		return btnDecrypt;
	}

	public JLabel getLblMacAddress() {
		if (lblMacAddress == null) {
			lblMacAddress = new JLabel("Mac address:");
			lblMacAddress.setBounds(10, 76, 79, 14);
		}
		return lblMacAddress;
	}

	public JTextField getTxtMac() {
		if (txtMac == null) {
			txtMac = new JTextField();
			txtMac.setEditable(false);
			txtMac.setBounds(88, 73, 298, 20);
			txtMac.setColumns(10);
		}
		return txtMac;
	}

	public JLabel getLblNewLabel() {
		if (lblNewLabel == null) {
			lblNewLabel = new JLabel("Expiredate:");
			lblNewLabel.setBounds(10, 106, 68, 14);
		}
		return lblNewLabel;
	}

	public JTextField getTxtExpiredate() {
		if (txtExpiredate == null) {
			txtExpiredate = new JTextField();
			txtExpiredate.setEditable(false);
			txtExpiredate.setBounds(88, 103, 295, 20);
			txtExpiredate.setColumns(10);
		}
		return txtExpiredate;
	}
}
