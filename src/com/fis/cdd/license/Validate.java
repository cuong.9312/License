package com.fis.cdd.license;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validate {
	private static final String MAC_PATTERN = "^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})$";

	public static boolean validateMAC(String mac) {
		Pattern pattern = Pattern.compile(MAC_PATTERN);
		Matcher matcher = pattern.matcher(mac);
		return matcher.matches();
	}

	public static void main(String[] args) {
		System.out.println(validateMAC("12-12-12-12-12-12e"));
	}
}
