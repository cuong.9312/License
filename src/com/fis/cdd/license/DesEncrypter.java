package com.fis.cdd.license;

import java.util.Calendar;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

public class DesEncrypter {
	Cipher ecipher;

	Cipher dcipher;

	public DesEncrypter(SecretKey key) {
		try {
			ecipher = Cipher.getInstance("DES");
			dcipher = Cipher.getInstance("DES");
			ecipher.init(Cipher.ENCRYPT_MODE, key);
			dcipher.init(Cipher.DECRYPT_MODE, key);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public DesEncrypter() {
		try {
			SecretKey key = ConvertKeyString.stringToSecretkey("Dcj3vNzLURA=");
			ecipher = Cipher.getInstance("DES");
			dcipher = Cipher.getInstance("DES");
			ecipher.init(Cipher.ENCRYPT_MODE, key);
			dcipher.init(Cipher.DECRYPT_MODE, key);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public String encrypt(String str) throws Exception {
		// Encode the string into bytes using utf-8
		byte[] utf8 = str.getBytes("UTF8");

		// Encrypt
		byte[] enc = ecipher.doFinal(utf8);

		// Encode bytes to base64 to get a string
		return new sun.misc.BASE64Encoder().encode(enc);
	}

	public String decrypt(String str) {
		try {
			// Decode base64 to get bytes
			byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);

			byte[] utf8 = dcipher.doFinal(dec);

			// Decode using utf-8
			return new String(utf8, "UTF8");
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static void main(String[] argv) throws Exception {
		DesEncrypter encrypter = new DesEncrypter();
		String decrypted = encrypter.decrypt("gK8GzOpKpwYmv1dtcYe5BpdWS9XYKrzz");
		System.out.println("decrypted: " + decrypted);
	}

	public String generateKey(String mac, int date) {
		try {
			Calendar calendar = Calendar.getInstance();
			calendar.add(Calendar.DAY_OF_MONTH, date);

			String expiredate = calendar.get(Calendar.DAY_OF_MONTH) + "_" + calendar.get(Calendar.MONTH) + "_"
					+ calendar.get(Calendar.YEAR);
			return encrypt(mac + "_" + expiredate);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
}
