package com.fis.cdd.license;

import java.util.Calendar;
import java.util.StringTokenizer;

public class Parser {
	public static boolean validateKeyGen(String keyGen) {
		try {
			DesEncrypter encrypter = new DesEncrypter();
			String str = encrypter.decrypt(keyGen);
			System.out.println(str);
			StringTokenizer stk = new StringTokenizer(str, "_");
			String macAddress = stk.nextToken();
			System.out.println(macAddress);
			String dayOfMonth = stk.nextToken();
			System.out.println(dayOfMonth);
			String month = stk.nextToken();
			System.out.println(month);
			String year = stk.nextToken();
			System.out.println(year);

			String macLocal = GetMacAddress.getMac();
			if (!macLocal.equals(macAddress)) {
				System.out.println("mac not equal");
				return false;
			}
			Calendar expriredate = Calendar.getInstance();
			expriredate.set(Calendar.DAY_OF_MONTH, Integer.parseInt(dayOfMonth));
			expriredate.set(Calendar.MONTH, Integer.parseInt(month));
			expriredate.set(Calendar.YEAR, Integer.parseInt(year));
			Calendar dateNow = Calendar.getInstance();
			if (expriredate.before(dateNow)) {
				return false;
			}
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
			return false;
		}
	}

	public static void main(String[] args) {
		if (validateKeyGen("gK8GzOpKpwYmv1dtcYe5BjJe6GhJZaObjWhVbaZYhb0=")) {
			System.out.println("valid");
		} else {
			System.out.println("invalid");
		}
	}
}
