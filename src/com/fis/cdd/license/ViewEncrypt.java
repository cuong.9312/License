package com.fis.cdd.license;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.crypto.SecretKey;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.UIManager;

public class ViewEncrypt {

	private JFrame frmGenerateKey;
	private JLabel lblMacAddress;
	private JTextField txtMacAddress;
	private JLabel lblDate;
	private JComboBox cbDate;
	private JButton btnGenerator;
	private JTextField txtKeyGen;
	private JLabel lblDays;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
					ViewEncrypt window = new ViewEncrypt();
					window.frmGenerateKey.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ViewEncrypt() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		this.frmGenerateKey = new JFrame();
		this.frmGenerateKey.setTitle("Generate key");
		this.frmGenerateKey.setBounds(100, 100, 450, 170);
		this.frmGenerateKey.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.frmGenerateKey.getContentPane().setLayout(null);
		this.frmGenerateKey.getContentPane().add(getLblMacAddress());
		this.frmGenerateKey.getContentPane().add(getTxtMacAddress());
		this.frmGenerateKey.getContentPane().add(getLblDate());
		this.frmGenerateKey.getContentPane().add(getCbDate());
		this.frmGenerateKey.getContentPane().add(getBtnGenerator());
		this.frmGenerateKey.getContentPane().add(getTxtKeyGen());
		this.frmGenerateKey.getContentPane().add(getLblDays());
	}

	public JLabel getLblMacAddress() {
		if (lblMacAddress == null) {
			lblMacAddress = new JLabel("Mac address:");
			lblMacAddress.setBounds(10, 11, 88, 14);
		}
		return lblMacAddress;
	}

	public JTextField getTxtMacAddress() {
		if (txtMacAddress == null) {
			txtMacAddress = new JTextField();
			txtMacAddress.setBounds(117, 8, 307, 20);
			txtMacAddress.setColumns(10);
		}
		return txtMacAddress;
	}

	public JLabel getLblDate() {
		if (lblDate == null) {
			lblDate = new JLabel("Date:");
			lblDate.setBounds(10, 47, 46, 14);
		}
		return lblDate;
	}

	public JComboBox getCbDate() {
		if (cbDate == null) {
			cbDate = new JComboBox();
			cbDate.setModel(new DefaultComboBoxModel(new String[] { "5", "15", "30", "60", "90", "180", "1000" }));
			cbDate.setBounds(117, 44, 88, 20);
		}
		return cbDate;
	}

	public JButton getBtnGenerator() {
		if (btnGenerator == null) {
			btnGenerator = new JButton("Generator");
			btnGenerator.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					String macAddress = txtMacAddress.getText();
					if (!Validate.validateMAC(macAddress)) {
						JOptionPane.showMessageDialog(null, "mac address is invalid!");
					} else {
						int date = Integer.parseInt(cbDate.getSelectedItem().toString());
						SecretKey secretKey = ConvertKeyString.stringToSecretkey("Dcj3vNzLURA=");
						DesEncrypter encrypter = new DesEncrypter();
						String keyGen = encrypter.generateKey(macAddress, date);
						txtKeyGen.setText(keyGen);
					}
				}
			});
			btnGenerator.setBounds(10, 82, 89, 23);
		}
		return btnGenerator;
	}

	public JTextField getTxtKeyGen() {
		if (txtKeyGen == null) {
			txtKeyGen = new JTextField();
			txtKeyGen.setEditable(false);
			txtKeyGen.setBounds(117, 83, 307, 20);
			txtKeyGen.setColumns(10);
		}
		return txtKeyGen;
	}

	public JLabel getLblDays() {
		if (lblDays == null) {
			lblDays = new JLabel("days");
			lblDays.setBounds(215, 47, 46, 14);
		}
		return lblDays;
	}

}
