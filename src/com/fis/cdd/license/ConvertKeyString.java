package com.fis.cdd.license;

import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class ConvertKeyString {
	public static String secretKeyToString(SecretKey secretKey) {
		try {
			// get base64 encoded version of the key
			String encodedKey = Base64.getEncoder().encodeToString(secretKey.getEncoded());
			return encodedKey;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static SecretKey stringToSecretkey(String encodedKey) {
		try {
			// decode the base64 encoded string
			byte[] decodedKey = Base64.getDecoder().decode(encodedKey);
			// rebuild key using SecretKeySpec
			SecretKey originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "DES");
			return originalKey;
		} catch (Exception ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static void main(String args[]) throws NoSuchAlgorithmException {
		// create new key
		SecretKey secretKey = KeyGenerator.getInstance("DES").generateKey();
		String key = secretKeyToString(secretKey);
		System.out.println(key);
	}
}
